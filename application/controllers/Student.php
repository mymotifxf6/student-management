<?php

class Student extends CI_Controller{

	function index() {
		$this->load->model('Student_model');
		$students = $this->Student_model->all();
		$data = array();
		$data['students'] = $students;
		$data['subjects'] = array("Commercial Studies", "Economics", "Mathematics", "Environmental Science", "Physics", "Chemistry", "Biology", "History", "Civics", "Geography");
		$this->load->view('student/index', $data);
	}

	function create() {
		$data = array();
		$data['subjects'] = array("Commercial Studies", "Economics", "Mathematics", "Environmental Science", "Physics", "Chemistry", "Biology", "History", "Civics", "Geography");
		$this->load->model('Student_model');
		$this->form_validation->set_rules('firstname', 'Firstname', 'required');
		$this->form_validation->set_rules('lastname', 'Lastname', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('stdclass', 'Stdclass', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('student/create', $data);
		} else {
			$formdata = array();
			$formdata['firstname'] = $this->input->post('firstname');
			$formdata['lastname'] = $this->input->post('lastname');
			$formdata['gender'] = $this->input->post('gender');
			$formdata['stdclass'] = $this->input->post('stdclass');
			$formdata['subjects'] = implode(',', $this->input->post('subjects'));
			$formdata['created_at'] = date('Y-m-d');

			$this->Student_model->store($formdata);
			$this->session->set_flashdata('success', 'Record added successfully');
			redirect(base_url().'index.php/student/');
		}
	}

	function edit($id) {
		$this->load->model('Student_model');
		$student = $this->Student_model->getStudent($id);
		$subSel = explode(',', $student['subjects']);

		$data = array();
		$data['student'] = $student;
		$data['subjects'] = array("Commercial Studies", "Economics", "Mathematics", "Environmental Science", "Physics", "Chemistry", "Biology", "History", "Civics", "Geography");
		$data['subSel'] = $subSel;

		$this->form_validation->set_rules('firstname', 'Firstname', 'required');
		$this->form_validation->set_rules('lastname', 'Lastname', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('stdclass', 'Stdclass', 'required');

		if ($this->form_validation->run() == false) {
			$this->load->view('student/edit', $data);
		} else {
			$formdata = array();
			$formdata['firstname'] = $this->input->post('firstname');
			$formdata['lastname'] = $this->input->post('lastname');
			$formdata['gender'] = $this->input->post('gender');
			$formdata['stdclass'] = $this->input->post('stdclass');
			$formdata['subjects'] = implode(',', $this->input->post('subjects'));
			$formdata['created_at'] = date('Y-m-d');

			$this->Student_model->updateStudent($id, $formdata);
			$this->session->set_flashdata('success', 'Record updated successfully');
			redirect(base_url().'index.php/student/');
		}
	}

	function delete($id) {
		$this->load->model('Student_model');
		$student = $this->Student_model->getStudent($id);
		if(empty($student)) {
			$this->session->set_flashdata('failure', 'Record not found in database');
			redirect(base_url().'index.php/student/');
		}

		$this->Student_model->deleteStudent($id);
		$this->session->set_flashdata('success', 'Record deleted successfully');
		redirect(base_url().'index.php/student/');
	}
}
