<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	</head>
	<body>
		<div class="navbar navbar-dark bg-dark">
			<div class="container">
				<a href="#" class="navbar-brand">Student Management</a>
			</div>
		</div>
		<div class="container" style="padding: 10px;">
			<div class="row">
				<div class="col-md-6"><h3>Create Student</h3></div>
			</div>

			<hr>
			<form method="post" name="createStudent" action="<?= base_url(). 'index.php/student/create' ?>">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-md-6">
						<div class="form-group">
							<label for="firstname">First Name</label>
							<input type="text" name="firstname" id="firstname" class="form-control">
							<?php echo form_error('firstname'); ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="lastname">Last Name</label>
							<input type="text" name="lastname" id="lastname" class="form-control">
							<?php echo form_error('lastname'); ?>
						</div>
					</div>
				</div>
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-md-4">
						<div class="form-group">
							<fieldset class="form-group">
								<div class="row">
									<legend class="col-form-label col-sm-2 pt-0">Radios</legend>
									<div class="col-sm-10">
										<div class="form-check">
											<input class="form-check-input" type="radio" name="gender" id="female" value="Female">
											<label class="form-check-label" for="female">
												Female
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" type="radio" name="gender" id="male" value="Male">
											<label class="form-check-label" for="male">
												Male
											</label>
										</div>
									</div>
								</div>
								<?php echo form_error('gender'); ?>
							</fieldset>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="stdclass">Class</label>
							<input type="text" name="stdclass" id="stdclass" class="form-control">
							<?php echo form_error('stdclass'); ?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="subjects">Subjects</label>
							<select name="subjects[]" id="subjects" class="selectpicker form-control" data-live-search="true" multiple>
								<?php
									if (!empty($subjects)) {
										foreach($subjects as $key=>$val) {
											echo '<option value="'.$key.'">'.$val.'</option>';
										}
									}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Create</button>
						<a href="<?= base_url(). 'index.php/student/' ?>" class="btn-secondary btn">Cancel</a>
					</div>
				</div>
			</form>

		</div>
	</body>
</html>
