<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
</head>
<body>
<div class="navbar navbar-dark bg-dark">
	<div class="container">
		<a href="#" class="navbar-brand">Student Management</a>
	</div>
</div>
<div class="container" style="padding: 10px;">
	<div class="row">
		<div class="col-md-12">
			<?php
				$success = $this->session->userdata('success');
				$error = $this->session->userdata('failure');
				if($success != "") {
					echo '<div class="alert alert-success">' . $success . '</div>';
				}
				if($error != "") {
					echo '<div class="alert alert-danger">' . $error . '</div>';
				}
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6"><h3>List of Student</h3></div>
		<div class="col-md-6" style="text-align: right"><a class="btn btn-success" href="create">Create</a></div>
	</div>

	<hr>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped">
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Gender</th>
					<th>Class</th>
					<th>Subjects</th>
					<th>Action</th>
				</tr>
				<?php if(!empty($students)) { foreach ($students as $student) { ?>
					<tr>
						<td><?= $student['firstname'] ?></td>
						<td><?= $student['lastname'] ?></td>
						<td><?= $student['gender'] ?></td>
						<td><?= $student['stdclass'] ?></td>
						<td>
							<?php
								$selectedSubs = array();
								$subjSels = explode (',', $student['subjects']);
								foreach($subjects as $key=>$val) {
									if (in_array($key, $subjSels)) {
										array_push($selectedSubs, $subjects[$key]);
									}
								}
								echo implode(", ", $selectedSubs);
							?>
						</td>
						<td>
							<a href="<?php echo base_url(). 'index.php/student/edit/' . $student['id'] ?>"><i class="bi bi-pencil-square"></i></a> |
							<a href="<?php echo base_url(). 'index.php/student/delete/' . $student['id'] ?>"><i style="color: red" class="bi bi-trash-fill"></i></a>
						</td>
					</tr>
				<?php }} else { ?>
					<tr><td>No records found</td></tr>
				<?php } ?>
			</table>
		</div>
	</div>

</div>
</body>
</html>
