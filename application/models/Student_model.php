<?php

class Student_model extends CI_Model {

	function store($formData) {
		$this->db->insert('students', $formData);
	}

	function all() {
		return $students = $this->db->get('students')->result_array();
	}

	function getStudent($id) {
		$this->db->where('id', $id);
		return $this->db->get('students')->row_array();
	}

	function updateStudent($id, $formData) {
		$this->db->where('id', $id);
		$this->db->update('students', $formData);
	}

	function deleteStudent($id) {
		$this->db->where('id', $id);
		$this->db->delete('students');
	}
}
